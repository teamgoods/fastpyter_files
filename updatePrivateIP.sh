HOMEDIR=/home/aidemy
sudo cat $HOMEDIR/setup/jupyterconfig.py > $HOMEDIR/.jupyter/jupyter_notebook_config.py
privateip=$(echo $(hostname -I) | tr -d '[:space:]')
sudo sed -i -e "s/<<privateip>>/$privateip/g" $HOMEDIR/.jupyter/jupyter_notebook_config.py