result=$(sudo parted -l | grep unknown)
if [ "$result" = "" ]; then
  echo "フォーマットされています"
else
  echo "フォーマットされていません。フォーマットします。"
  sudo mkfs -t ext4 /dev/disk/azure/scsi1/lun0
fi
sudo mkdir /fileshare
sudo mount /dev/disk/azure/scsi1/lun0 /fileshare