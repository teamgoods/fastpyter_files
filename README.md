# fastpyter_files

This repo is a read-only repo for fastpyter setup.

# how to create image
1. ubuntuのvmを立てる（一番しょぼいやつだとメモリが足りなくなるので、サイズを１段階いいやつにする）
1. scpでもvmでもなんでもいいのでどうにかしてホームディレクトリにprepare.shを配置する
1. ホームディレクトリでprepare.shを実行する
1. `sudo apt-get install nginx` でnginxをインストールする
1. nginxのコンフィグファイルを/etc/nginx/conf.d/の中に配置する
1. vimを利用して、/etc/nginx/sites-enabled/defaultの中にある `default server`という記述を削除する。
1. `sudo systemctl restart nginx` でnginxを再起動
1. `sudo su` でroor権限を取得
1. `pip install -r ~/setup/piplist.txt` でpythonのパッケージをインストール
1. `exit` でroot権限から抜ける
1. `sudo systemctl restart jupyter` でjupyter notebookを再起動
1. この状態でAzure上からvmのイメージを作成