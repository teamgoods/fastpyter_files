FROM ubuntu:16.04

ARG HOMEDIR="/home/aidemy"
ARG PYTHONVER="3.6.3"
ARG PYTHONIOENCODING="utf-8"

RUN mkdir ${HOMEDIR}
RUN mkdir ${HOMEDIR}/setup
RUN mkdir ${HOMEDIR}/.jupyter

COPY ./integrated/custom.js ${HOMEDIR}y/setup
COPY ./integrated/jupyter ${HOMEDIR}/setup
COPY ./integrated/jupyter.service ${HOMEDIR}/setup
COPY ./integrated/jupyterconfig.py ${HOMEDIR}/setup
COPY ./integrated/piplist.txt ${HOMEDIR}/setup

RUN apt update -yq && \
apt upgrade -yq && \
apt install -yq pkg-config --fix-missing --no-install-recommends \
wget \
openssh-client \
git \
build-essential \
gcc \
python3-dev \
zlib1g-dev \
libffi-dev \
libssl-dev \
libpng12-dev \
libfreetype6-dev \
libmecab-dev \
libopencv-dev \
libbz2-dev \
libsqlite3-dev \
sqlite3 \
tk-dev \
python3-tk \
graphviz \
vim \
mecab \
mecab-ipadic-utf8 \
language-pack-ja \
fonts-ipa* \
language-pack-ja-base \
language-pack-ja \
python-pip \
python3-pip && \
apt clean && \
rm -rf /var/lib/apt/lists/* && \
locale-gen ja_JP.UTF-8

ENV LANG ja_JP.UTF-8
ENV LANGUAGE ja_JP:ja
ENV LC_ALL ja_JP.UTF-8

RUN wget https://www.python.org/ftp/python/${PYTHONVER}/Python-${PYTHONVER}.tgz --no-check-certificate && \
tar zxf Python-${PYTHONVER}.tgz && \
cd /Python-${PYTHONVER} && \
./configure && \
make -j8 && \
make install && \
pip3 install --upgrade pip && \
pip3 --no-cache-dir install -U -r ${HOMEDIR}/setup/piplist.txt && \
pip install jupyter && \
cd ../ && \
rm -rf ./Python-${PYTHONVER}.tgz ./Python-${PYTHONVER} get-pip.py

# ここまでpythonsetup.sh

RUN jupyter notebook --generate-config
RUN cat ${HOMEDIR}/setup/jupyterconfig.py > ${HOMEDIR}/.jupyter/jupyter_notebook_config.py
RUN privateip=$(echo $(hostname -I) | tr -d '[:space:]') && \
sed -i -e "s/<<privateip>>/$privateip/g" $HOMEDIR/.jupyter/jupyter_notebook_config.py