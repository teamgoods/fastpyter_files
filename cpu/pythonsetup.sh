#!/bin/bash
sudo apt install -yq pkg-config --fix-missing \
wget \
openssh-client \
git \
build-essential \
gcc \
python3-dev \
zlib1g-dev \
libffi-dev \
libssl-dev \
libpng12-dev \
libfreetype6-dev \
libmecab-dev \
libopencv-dev \
libbz2-dev \
libsqlite3-dev \
sqlite3 \
tk-dev \
python3-tk \
graphviz \
vim \
mecab \
mecab-ipadic-utf8 \
language-pack-ja \
fonts-ipa*

# install python
PYTHONVER="3.6.3"
PYTHONIOENCODING="utf-8"
sudo wget https://www.python.org/ftp/python/${PYTHONVER}/Python-${PYTHONVER}.tgz
tar zxf Python-${PYTHONVER}.tgz
cd Python-${PYTHONVER}
./configure
make -j8
make install
# install pip
sudo wget https://bootstrap.pypa.io/get-pip.py && sudo python3 get-pip.py
# install libraries
sudo pip --no-cache-dir install -U -r ~/setup/piplist.txt
sudo pip install --upgrade pip
sudo pip install jupyter
# clean directory
sudo rm -rf ./Python-${PYTHONVER}.tgz ./Python-${PYTHONVER} get-pip.py