#!/bin/bash
HOMEDIR=/home/aidemy
echo "Running setup script"
# Update VM itself first
sudo apt update -yq
sudo apt upgrade -yq

# Make jupyter root folder
mkdir $HOMEDIR/.jupyter
# Run python setup script 
bash $HOMEDIR/setup/pythonsetup.sh
# Create jupyter config
jupyter notebook --generate-config
# Append jupyter config from prepared file
cat $HOMEDIR/setup/jupyterconfig.py > $HOMEDIR/.jupyter/jupyter_notebook_config.py
# update privateip
privateip=$(echo $(hostname -I) | tr -d '[:space:]')
sed -i -e "s/<<privateip>>/$privateip/g" $HOMEDIR/.jupyter/jupyter_notebook_config.py

# Copy the Jupyter service file 
sudo cp $HOMEDIR/setup/jupyter.service /etc/systemd/system/
sudo cp $HOMEDIR/setup/jupyter /etc/sysconfig/
# Copy Jupyter custom file (for single tab mode)
sudo mkdir $HOMEDIR/.jupyter/custom
sudo cp $HOMEDIR/setup/custom.js $HOMEDIR/.jupyter/custom/custom.js

# Install libraries for gym
apt-get install -y python-pyglet python3-opengl zlib1g-dev libjpeg-dev patchelf cmake swig libboost-all-dev libsdl2-dev libosmesa6-dev xvfb ffmpeg

# Install protoc command
sudo apt-get install -y protobuf-compiler

# Setup Jupyter.service for system startup restart
sudo chown aidemy:aidemy $HOMEDIR/.jupyter
sudo systemctl enable jupyter.service
sudo systemctl daemon-reload
sudo systemctl restart jupyter.service

## EXPERIMENTAL
# Setup cron job for piplist update
#crontab -l > $HOMEDIR/update/cronjoblist
#echo "00 04 * * * $HOMEDIR/update/updatecheck.sh" >> $HOMEDIR/update/cronjoblist
#crontab $HOMEDIR/update/cronjoblist
#rm $HOMEDIR/update/cronjoblist
#rm /home/aidemy/setup -r

# Clean up
# rm $HOMEDIR/update