#!/bin/bash
HOMEDIR=/home/aidemy
type=$1
url=https://bitbucket.org/teamgoods/fastpyter_files/raw/master/$type.txt
files=$(curl $url -s)
for fname in $files;
do
 wget --no-cache --debug https://bitbucket.org/teamgoods/fastpyter_files/raw/master/$fname -P $HOMEDIR/setup
done

# Finally, run the main setup script
sudo bash $HOMEDIR/setup/setup.sh
